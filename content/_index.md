


## Documents pour la première NSI
1. [Les bases de Python 3]({{< ref "docs/1_python.md"  >}})
2. [Algorithmes: parcours séquentiel et recherche par dichotomie]({{< ref  "/docs/algo1.md">}})
3. [Algorithmes avancés: algorithmes gloutons et algorithmes des $k$ plus proches voisins]({{< ref  "docs/algo2.md">}}) 
4. [Données en tables]({{< ref  "docs/donnees_tables.md">}})
5. [Binaire: entier, entiers signés, flottants, caractères]({{< ref "docs/binaire.md" >}})
6. [Systèmes d'exploitation - ligne de commande]({{< ref "/docs/bash.md">}})
7. [Architecture logique d'un ordinateur]({{< ref  "docs/architecture.md" >}})
8. * [Le Web (base du HTML , css et javaScript)]({{< ref "docs/web.md">}})
   * [Réseau ( notion de réseau physique, encapsulation et bit alterné]({{< ref "docs/reseau.md">}})


## Python 


*  [Thonny](https://thonny.org/) Python pour débutant utilisable sur tous les OS.
*  [EduPython](https://edupython.tuxfamily.org/) *pour Windows et existe en version portable* 
*  [Edupyter](https://www.edupyter.net/) application pour *Windows* contenant Python, JupyterLab, Jupyter notebook et thonny. S'installe sans droit administrateur.
*  [Jupyter Lab app](https://github.com/jupyterlab/jupyterlab_app#download)
*  [Basthon](https://basthon.fr/)  *un site pour tester des programmes Python ou des Notebooks Jupyter en ligne.* 
    * [Console Basthon sur ce site](basthon-console)
    * [Notebook Basthon sur ce site](basthon-notebook)
*  [Python tutor](http://pythontutor.com/visualize.html#mode=edit) pour visualiser un court programme pas à pas.
    * [Notebook: Utiliser python tutor](https://flaustriat.frama.io/basthon-notebook/?from=/python/pythonTutor.ipynb)
*  [Js bin](https://jsbin.com/?html,css,js,output) pour tester HTML, CSS et Javascript.
*  [https://glitch.com/](https://glitch.com/) pour tester un serveur Flask.
*   [WebVIM: une distribution linux Debian dans le navigateur](https://webvm.io/) 

## Ressources
* [Sitographie]({{< ref "site.md" >}})
* [Raccourcis clavier - obtenir les caractères spéciaux]({{< ref "clavier.md">}})
