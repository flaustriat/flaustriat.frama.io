---
subtitle: "Débuter avec Python 3"
tags: [Python, Python 3]
---

### Cours en ligne: les bases de Python 3

#### Les bases de Python (calculs, types de base, fonctions)

* * [TP Les bases de Python](http://prof.math.free.fr/nsi/premiere/python1.html#haut)  sur le site [prof.math.free.fr](http://prof.math.free.fr/nsi/premiere/python1.html#haut) 
  * [TP les bases de Python](https://progalgo.fr/) sur le site https://progalgo.fr/

* [Cours base de Python](/python/basesPython.pdf)

  * [TD calculs et notion de variable](/python/TD-NotionVariablesCalculs.pdf) 

* [Cours : fonctions: jeu  de tests et préconditions](/python/fonction-jeuTestsPrécondition.pdf)

  * [TD notions de fonctions(pdf)](/python/TD-notionsFonctions.pdf)

* [TD nombres et variables (pdf)](/python/TD_nombresVariables.pdf)

* [Utiliser Python Tutor](https://flaustriat.frama.io/basthon-notebook/?from=/python/pythonTutor.ipynb)
  
  
  


#### Structures conditionnelles

* [Cours structures conditionnelles](/python/StructuresConditionnelles.pdf) 

* [TD structures conditionnelles](/python/TD-structuresConditionnelles.pdf)

  
#### Chaînes de caractères

* [Cours chaînes de caractères](/python/ChaînesDecaracteres.pdf)
  
  

#### Boucles bornées

* [Cours boucles bornées](/python/boucleFor.pdf)
* [TD boucles bornées](/python/TD_boucle_for.pdf)

#### Boucles non bornées

* [Cours boucles non bornées](/python/boucleWhile.pdf)
* [TD boucles non bornées (pdf)](/python/TD-bouclesWhile.pdf)

#### Tableaux et tuples

* [Cours tableaux et tuples](/python/TableauxTuples.pdf)
* [TD tableaux et tuples (pdf)](/python/TD_TableauxTuples.pdf) 

#### Tableaux et accumulateurs

* [Cours parcours de tableaux(pdf)](/python/parcoursTableaux.pdf)

#### Tableaux par compréhension

* [Cours tableaux par compréhension(pdf)](/python/tableauxCompréhension.pdf)
* [QCM tableaux par compréhension(pdf)](/python/TD-compréhension.pdf)

#### Dictionnaires

* [Cours dictionnaires](/python/coursDictionnaires.pdf)

* [QCM dictionnaires (pdf)](/python/QCM-dictionnaires.pdf)

* [TP Json et API](https://flaustriat.frama.io/basthon-notebook/?from=/python/TP_API/TP_JSON_API.ipynb&aux=/python/TP_API/exemple_json.json&aux=/python/TP_API/exemple_json2.json) 

  
  
#### Boucles imbriquées
* [Cours boucles imbriquées](https://flaustriat.frama.io/basthon-notebook/?from=/python/cours_boucles_imbriques.ipynb)
  
* 
  
  



#### Listes par compréhension - tableau de tableaux

* [Cours listes par compréhension et tableau de tableaux](/python/listesParCompréhension.pdf)
* [TP Listes par compréhension](/python/TP-listes-compréhension.pdf)
* [TP tableau de tableaux](/python/TP-tableauDeTableaux.pdf)



#### [Menento Python](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)

[De courts exemples de code en Python](http://lycee.educinfo.org/python-snippets/) 

___

#### Cours 2020-2021

**Base de Python**

* [TP premiers programmes (pdf)](/python/TP_premiersProgrammes.pdf)
* [TP fonctions (pdf)](/python/TP_fonctions.pdf)
* [Cours booléens](/python/Cours_booleens.pdf)
* [TP booléens](/python/TD_booleens.pdf)

**Structures conditionnelles**

[TP tests et conditions (pdf)](/python/TP_tests_conditions.pdf) 

------



#### Cours 2019-2020

Les liens "Binder" pointent vers des fichiers interactifs jupyter notebook.
Pour exécuter le contenu d'une cellule  d'un notebook taper `shift`+`entrée`

* Partie 1: Les nombres et les variables

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F1_nombres%2FdecouvertePython.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/python/1_nombres/decouvertePython.ipynb)
  * [Fiche TD](/python/1_nombres/TD_nombresVariables.pdf)
  * [Fiche TP](/python/1_nombres/TP_nombres.pdf)

* Partie 2: Premier programme

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F2_premiers_programmes%2Fpremier_programme.ipynb)
  * [Premier programme correction](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/2_premiers_programmes/premier_programme_corr.ipynb)
  * [Fiche TP](/python/1_decouverte_python/2_premiers_programmes/TP_premiers_programmes.pdf)

* Partie 3: Notion de fonction

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F3_notion_fonction%2Fdecouverte_python_fonctions.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/3_notion_fonction/decouverte_python_fonctions.ipynb)
  * [Fiche TP](/python//1_decouverte_python/3_notion_fonction/TP_fonctions.pdf)

* Partie 4: Chaînes de caractères et booléens

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F4_booleens_string%2FdecouvertePython_bool_string.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/4_booleens_string/decouvertePython_bool_string.ipynb)
  * [Fiche TP](/python//1_decouverte_python/4_booleens_string/TD_booleens.pdf)

* Partie 5: Conditions et branchements 

  *  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F5_conditions%2FdecouvertePythonConditions.ipynb)
  *  [cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/5_conditions/decouvertePythonConditions.ipynb)
  *  [Fiche TP](/python//1_decouverte_python/5_conditions/TP_tests_conditions.pdf)

* Partie 6: Boucles "Tant que"

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F6_boucles_tant_que%2Fdecouverte_python_boucle_tant_que.ipynb))

  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/6_boucles_tant_que/decouverte_python_boucle_tant_que.ipynb)

  * [Fiche TP (pdf)](/python/6_boucles_tant_que/TP_tant_que.pdf)


* Partie 7: Listes

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F7_listes%2Fliste_python1.ipynb)

  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/7_listes/liste_python1.ipynb)

* Partie 8: Boucles "Pour"

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F8_boucles_for%2Fboucle_for.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/8_boucles_for/boucle_for.ipynb)

* Partie 9: Tuple dictionnaire

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F9_tuple_dict%2Ftuple-dict.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/9_tuple_dict/tuple-dict.ipynb)

* Partie 10: Listes par compréhension -listes de listes / matrices

  * [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F10_listes_comprehension%2Fliste2_liste_comprehension.ipynb)
  * [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/10_listes_comprehension/liste2_liste_comprehension.ipynb)

  