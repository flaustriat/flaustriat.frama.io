---
subtitle: "Algorithme 1"
tags: [parcours séquentiel,parcours par dichotomie, tri par  sélection, tri par comparaison]
---



# Algorithme 1



[Notion de complexité](https://flaustriat.frama.io/basthon-notebook/?from=/algo1/notionComplexite.ipynb)



## Parcours séquentiel d'un tableau
* [Cours parcours séquentiel](/algo1/parcoursSéquentiel.pdf)
* [TD parcours séquentiel (pdf)](/algo1/TD-parcoursSeq.pdf)
* [TP parcours séquentiel (Pour visualiser avec PythonTutor)](https://flaustriat.frama.io/basthon-notebook/?from=/algo1/TD-parcoursSeq.ipynb) 

## Recherche par dichotomie

* [Cours recherche par dichotomie](/algo1/rechercheDichotomie.pdf)
* [Visualiser les exemples du cours avec PythonTutor](https://flaustriat.frama.io/basthon-notebook/?from=/algo1/rechercheDichotomie.ipynb)
* [TD recherche par dichotomie](/algo1/TD-rechercheDicho.pdf)

## Deux algorithmes de tri

* [Visualisation de différents algorithmes de tri](https://visualgo.net/en/sorting)
* [Visualisation de différents algorithmes de tri 2](http://lwh.free.fr/pages/algo/tri/tri.htm)
* [Cours sur le tri par sélection et le tri par insertion](/algo1/algo1-2tris.pdf) 
* [Cours sur la complexité](/algo1/algo1-complexité.pdf) 
* [TD tris](/algo1//TP_tris.pdf)
* [Article sur les tris](https://interstices.info/les-algorithmes-de-tri/)
* [SMBC list](https://www.smbc-comics.com/comic/list) 
* [Accidentally Quadratic](https://accidentallyquadratic.tumblr.com/) 


------




#### Algorithme 1 - année 2019-2020

##### Recherche d'un élément dans une liste

###### Recherche séquentielle

* [TD](/algo1/algo1-rechercheSéquentielle.pdf)
* [noteBook](http://py.ac-paris.fr/0fe96009)
* Correction du [noteBook](/algo1/rechercheSequentielle.pdf)

##### Recherche dichotomique dans une liste triée

* [TD](/algo1/algo2-rechercheDicho.pdf)
* [noteBook](http://py.ac-paris.fr/6b4406b6)
* Correction du [noteBook](/algo1/rechercheDicho.pdf)

##### Tri d'une liste

* [Cours tri par insertion - tri par selection](/algo1/algo1-tris.pdf)
* [Visualitation de différents algorithmes de tri](http://lwh.free.fr/pages/algo/tri/tri.htm)
* [Visualisation de différents algorithmes de tri 2](http://inriamecsci.github.io/#!/grains/methodes-tri)
* [Article sur les tris](https://interstices.info/les-algorithmes-de-tri/)
* [noteBook tri par insertion](http://py.ac-paris.fr/b6f559dd)
* [Cours sur la complexité](/algo1/algo1-complexité.pdf)
* [noteBook comparaison de tris](http://py.ac-paris.fr/58f67057) 









