---
subtitle: "Algorithmes avancés"
tags: [algorithme, knn, k plus proches voisins, algorithmes gloutons]
---

# Algorithmes avancés

## *k* plus proches voisins.

* [Vidéo: Démystifier l'intelligence artificielle](https://www.youtube.com/watch?v=B64xi7y9UxA) 
* [Vidéo: IA apprentissage automatique](https://www.youtube.com/watch?v=qO00I8vU81A)
* [Article: Qu’est-ce que l’intelligence artificielle ? Yann LeCun](https://www.college-de-france.fr/site/yann-lecun/Recherches-sur-l-intelligence-artificielle.htm) 
* [BD: Intelligences artificielles: Miroirs de nos vies](https://www.editions-delcourt.fr/bd/series/serie-intelligences-artificielles/album-intelligences-artificielles) 
* [Vidéo: comprendre le Machine Learning: L'algorithme du KNN](https://www.youtube.com/watch?v=9pvbEP1eyNY&feature=youtu.be) 
* [Cours Knn](/algo2/algo2CoursKnn.pdf) 
  * [Image 1](/algo2/algo_1_sur_2.gif)
  * [Image 2](/algo2/algo_2_sur_2.gif) 
* [TD Knn](/algo2/exercicesKnn.pdf)
* [Le fichier iris](/algo2/iris.csv)
* [TP Knn le fichier Iris](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/7061-24916) 

## Algorithmes gloutons 

* [Le cours TD](/algo2/algo2-glouton.pdf)
* [Le TP rendu de monnaie](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/8ee7-22419) 
* [Animation d'introduction au problème du sac à dos](http://hmalherbe.fr/thalesm/gestclasse/documents/Premiere_NSI/Animations/sac_a_dos/p5.js/sac_a_dos.html)
* [Le TP problème du sac à dos](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a620-22416) 

