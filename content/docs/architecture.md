---
subtitle: "Architecture"
tags: [Von Neumann, assembleur, portes logiques]
---

### Architecture

#### Portes logiques

* [Cours portes logiques](/architecture/portes_logiques.pdf)
* [TD portes logiques](/architecture/TD-portesLogiques.pdf)

#### Architecture de Von Neumann

* [Cours architecture de Von Neumann](/architecture/ArchiVVonNeumann.pdf)
* [TD assembleur](/architecture/M99-élèves.pdf) 
* [Lien vers le M99](https://raw.githubusercontent.com/InfoSansOrdi/M999/master/M99-memoire.pdf)
* [Vidéo: Histoire de l'architecture des ordinateurs](https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs)



Pour aller plus loin:

* [Nand Game](https://nandgame.com/) Du transistor à l'ordinateur. Jeu de puzzle en anglais.
* [De la porte Nand à l'assembleur (jeu payant sur Steam)](https://store.steampowered.com/app/1444480/Turing_Complete/) 
* https://www.nand2tetris.org/ Cours  "From NAND 2 Tetris" auquel les deux jeux précédents rendent hommage.
* Le jeu d'arcade Atari Pong est un des premier jeu  électronique qui a eu beaucoup de succès au début des années 70. Il est constitué essentiellement de portes logiques et d'horloges reliées ensemble. Il est un précurseur des jeux utilisant des processeurs et des logiciels.
  * [Les circuits du jeu d'arcade Atari Pong](http://www.pong-story.com/LAWN_TENNIS.pdf)
  * [Simulation des circuits du jeu d'arcade Atari Pong](https://www.falstad.com/pong/)







