---
subtitle: "Systèmes d'exploitation - ligne de commande"
tags: [Bash, Linux, Systèmes d'exploitation]
---

# Systèmes d'exploitation

* __[Le cours](/bash/SystèmesExploitation.pdf)__
  Quelques vidéos sur les systèmes d'exploitation:

  * [Les systèmes d'exploitation](https://youtu.be/4OhUDAtmAUo)
  * [Histoire de Unix/Linux](https://youtu.be/bdSWj7Y50VY)


* __[Cours/TP ](/bash/LigneCommandes.pdf) sur la gestion des fichiers et des droits dans un système Linux__

* __[TP ligne de commandes](/bash/TP-ligneCommandes.pdf)__

Pour les TP si vous n'avez pas accès à un ordinateur sous Linux: 
[Terminal linux en ligne: http://s-macke.github.io/jor1k/demos/main.html](http://s-macke.github.io/jor1k/demos/main.html)



Dans ce cas remplacer dans les lignes utilisant `curl`remplacer   `curl`par `lynx`



* [BD de Aryana Peze: le terminal](https://blog.octo.com/bd-le-terminal/)
* [BD de Aryana Peze: les systèmes d'exploitation](https://blog.octo.com/bd-los/)
* [Le jeu Find Your Path](http://demo710.univ-lyon1.fr/FYP/) 
* [Le jeu terminus](https://luffah.xyz/bidules/Terminus/)
* [Le jeu Gameshell](https://github.com/phyver/GameShell)
* [Les jeux Wargames](https://overthewire.org/wargames/) Commencer par Bandit en anglais
* [The Command Line Murders](https://github.com/veltman/clmystery) en anglais
