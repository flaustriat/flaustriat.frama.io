---
subtitle: "Binaire, entiers, entiers signés, flottants, caractères"
tags: [entier, entiers signés, flottants, utf8, unicode]
---

## Avec des 0 et des 1

#### Les entiers

* [Cours entiers](/binaire/Cours-entiersPositifs.pdf)
* [TP nombres entiers (pdf) ](/binaire/TD_nombres_entiers.pdf)

#### Les entiers signés

* [Cours entiers signés](/binaire/Cours-entiersSignés.pdf) 
* [TP entiers signés](/binaire/TD_nombres_entiers_relatifs.pdf)

#### Les flottants

* [Cours flottants](/binaire/Cours-flottants.pdf)
* [TP flottants](/binaire/TP_flottants.pdf)

####  Le codage des caractères

* [Cours codage des caractères](/binaire/Cours-codageCaractères.pdf) 
* [Tableau ASCII](http://vivienfrederic.free.fr/ascii.pdf)
* [TP codage de caractères](/binaire/TP_caracteres.pdf)
* [Exemple justifiant qu'il ne faut pas utiliser utf-8 pour les noms des variables](https://flaustriat.frama.io/basthon-console/?script=eJw7t1_BVsGIl-vCPiBtzMt1DsQHEtoKhrxcBUWZeSUa5_ZrAgD4Jwz-) 



[Document sur des erreurs dépassement de capacités](/binaire/dépassementCapacité-Arrondis.pdf)