---
subtitle: "Données en tables"
tags: [CSV, data ]
---

## Données en tables

1. Fichiers CSV
   * [Attente en gare](/donnees_tables/DonnéesStructuréesCVS.pdf)
   * [Données sur les gares](/donnees_tables/gares-pianos.csv)
2. Traitement du fichier provenant de l'INSEE sur les prénoms
   * [TP sur les prénoms](/donnees_tables/TraitementDonnées1Prénoms.pdf)
   * [Données INSE sur les prénoms](/donnees_tables/nat2019.csv)
3. Fusion de tables
   * [TP sur la fusion de tables](/donnees_tables/TraitementDonnées2fusions2.pdf)
   * [Fichiers à télécharger](/donnees_tables/fichiers.zip)
   * [TP à faire en ligne sur Capytale avec le code 597a-16296](https://capytale.fr)
   * [TD tables](/donnees_tables/TDtables.pdf)

