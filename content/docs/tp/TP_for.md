La boucle for
=============

Pour chaque exercice, une fois que le programme fonctionne, le recopier
dans [PythonTutor](http://www.pythontutor.com/visualize.html#mode=edit)
à cette adresse: <http://www.pythontutor.com/visualize.html#mode=edit>
afin de pouvoir en visualiser l'exécution pas à pas.

Exercice
--------

Hugo souhaite placer une certaine somme d'argent sur un compte rémunéré.
Le capital ainsi placé augmente chaque année de 5 % (c'est-à-dire qu'il
est multiplié par $1,05$).

On veut écrire une fonction qui donne le capital disponible sur ce
compte au bout de la $n$-ième année. Voici l'algorithme qu'on se propose
d'utiliser:

``` {numbers="none"}
Fonction: calcul_capital
Paramètres:
     * 'capital_depart' un flottant
     * 'n' un entier
Résultat: un flottant qui est le capital disponible sur ce compte au bout de la n-ième année.
Exemple: Avec un capital de départ de 120 €, le capital disponible au bout de 8 ans sera d'environ 177,29 ( à un centime près).
Début
    capital prend la valeur capital_depart
    répeter n fois
        capital prend la valeur capital * 1,05
    renvoyer capital
Fin 
```

Donner une traduction en Python de cet algorithme. Vérifier qu'il
fonctionne correctement en utilisant l'exemple donné.

Exercice
--------

Écrire une procédure où est un entier qui affiche la table de
multiplication de jusqu'à $n \times m$ en utilisant une boucle for.
Reprendre l'algorithme du cours et le modifier. Exemple d'utilisation:

``` {.python language="Python" numbers="none"}
>>>table_mult(6,4)
1 fois 6 est égal à 6
2 fois 6 est égal à 12
3 fois 6 est égal à 18
4 fois 6 est égal à 24
```

Exercice
--------

Écrire une fonction où est un entier et qui retourne le produit
$\underbrace{2\times\dots\times2}_{ n \text{ fois}}$ Pour cela on va
utiliser le fait que l'on a:
$\begin{cases} 2^0 &=1 \\ 2^n &= 2 \times 2^{n-1} \text{ pour tout entier } n \end{cases}$.
Voici l'algorithme que l'on souhaite utiliser:

``` {numbers="none"}
Fonction: puiss2
Paramètres:
     * 'n' un entier
Résultat: un entier égal à 2^n  
    p = 1
    répéter n fois
        p prend la valeur 2 * p 
    renvoyer p
Fin 
```

Recopier puis compléter:

``` {.python language="Python"}
def puiss2(n):
    """
    int -> int
    n>=0
    Retourne le produit de 2 par lui-même n fois de suite
    """
   # À compléter
        
assert puiss2(0) == 1, "Test 1 doit retourner 1"
assert puiss2(2) == 4, "Test 2 doit retourner 4"
assert puiss2(10) == 1024, "Test 3 doit retourner 1024"
```

Exercice
--------

Écrire une procédure où est un entier plus grand ou égal à 1 qui affiche
de nombres aléatoires compris entre 1 et 6. Cela permet d'afficher le
résultat du lancer de dés biens équilibrés.

L'algorithme que l'on souhaite utiliser est le suivant:

``` {numbers="none"}
Fonction: hasard
Paramètres:
     * 'n' un entier supérieur ou égal à 1
Résultat: affiche n nombres aléatoires compris entre 1 et 6.
    répéter n fois
        affiche un nombre aléatoire compris entre 1 et 6
Fin 
```

Recopier puis compléter le script ci-dessous.

``` {.python language="Python"}
import random as rd
# Remarque rd.randint(1,6) retourne un entier compris entre 1 et 6
def hasard(n):
    """
    # À compléter
    """
    # À compléter
```

Exercice
--------

Écrire une fonction où est une chaine de caractères et est un caractère.
La fonction renvoie si le caractère est dans la chaîne de caractères
chaine et sinon.

L'algorithme que l'on souhaite utiliser est le suivant:

``` {numbers="none"}
Fonction: recherche
Paramètres:
     * 'chaine' une chaine de caractères
     * 'caract' un caractère
Résultat: un booléen. Retourne vrai si caract appartient à chaine, sinon retourne faux
    p = 1
    pour chaque caractère c de chaine
        si c est égal à caract
            retourner True
    Retourner False
Fin 
```

Recopier puis compléter le script suivant:

``` {.python language="Python"}
def recherche(chaine,caract):
    """
    string * string -> bool
    Retourne True si caract est dans chaine et False sinon.
    """
    # À compléter
    
## Tests unitaires
assert recherche("Bonjour","j") == True, "Test 1 doit retourner True"
assert recherche("Bonjour","b") == False, "Test 2 doit retourner False"
assert recherche("Bonjour","B") == True, "Test 3 doit retourner True"
```

Exercice
--------

Écrire une fonction qui compte le nombre de caractères dans la chaine de
caractères .

L'algorithme que l'on souhaite utiliser est le suivant:

``` {numbers="none"}
Fonction: occurence
Paramètres:
     * 'chaine' une chaine de caractères
     * 'caract' un caractère
Résultat: un entier. Retourne le nombre de fois ou est présent le caractère caract dans la chaine de caractères chaine.
    compteur = 0
    pour chaque caractère c de chaine
        si c est égal à caract
            compteur = compteur + 1
    Retourner compteur
Fin 
```

Recopier puis compléter le programme suivant:

``` {.python language="Python"}
def nb_occurence(chaine,caract):
    """
    # À compléter
    """
   # À compléter

assert nb_occurence("Bonjour","o") == 2 , "Test 1 doit retourner 2"
assert nb_occurence("Bonjour","w") == 0 , "Test 2 doit retourner 0"
assert nb_occurence("accaparat","a") == 4, "Test 3 doit retourner 4"
assert nb_occurence("Accaparat","a") == 3, "Test 4 doit retourner 3"
```

Exercice
--------

Écrire une fonction qui prend en argument une chaîne de caractères de
taille quelconque ne contenant que des \"0\" et des \"1\" et qui
représente un entier non signé écrit en binaire et qui renvoie cet
entier écrit en base 10. On rappelle qu'un entier écrit en binaire se
décompose en un somme de produit de 0 ou 1 par une puissance de 2. Par
exemple: $$101 = 1 \times 2^2 + 0 \times 2^1 + 1 \times 2^0 = 5$$ On
peut remarquer que le caractère le plus à gauche est 2 exposant la
longueur $-$ 1 puis qu'ensuite l'exposant décrémente de 1 pour chaque
caractère. L'idée de l'algorithme est d'initialiser une variable à 0 et
une variable à longueur de la chaine $-$1 puis de lire les caractères de
la chaine un par un. Si ce caractères est égal à \"1\" alors on ajoute à
, $\times 2^{i}$ puis de décrémenter de 1.

L'algorithme est le suivant:

``` {numbers="none"}
Fonction: bin2int
Paramètres:
     * 'chaine' une chaine de caractères. On suppose qu'elle ne contient que des "0" et des "1".

Résultat: un entier. Retourne L'écriture en base 10 de chaine qui doit être une chaine contenant un nombre écrit en base 2.
    nombre = 0
    i  est égal à la longueur de chaine -1
    pour chaque caractère c de chaine
        si c est égal à "1"
            nombre = nombre + 2^i
        i = i- 1
    Retourner nombre
Fin 
```

Recopier puis compléter le programme ci-dessous.

``` {.python language="Python"}
def bin2int(chaine):
    """
    string -> int
    chaine représente un nombre écrit en binaire. Sa taille est quelconque.
    On suppose qu'elle ne contient que des 0 et des 1.
    Convertie en base 10 le nombre représenté par la chaine et le retourne.
    """
    nombre = 0
    i = len(chaine) -1
    # À compléter

assert bin2int("10") == 2, "Test 1 doit retourner 2"
assert bin2int("11") == 3, "Test 2 doit retourner 3"
assert bin2int("100101000") == 296, "Test 3 doit retourner 296"
```
