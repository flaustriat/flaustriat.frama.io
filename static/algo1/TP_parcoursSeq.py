
##################
### EXERCICE 2
##################

def recherche_seq(tab,elem):
    for i in range(len(tab)):
        if tab[i] == elem:
            return ...
    return ...

assert recherche_seq([2,4,6,1],1)== 3
assert recherche_seq([7,4,2,9,8],4)==1
assert recherche_seq([7,4,2,9,8],3)==None


#################
### EXERCICE 4
#################

from timeit import timeit
import matplotlib.pyplot as plt
#Code de la fonction à tester
code ="""
def recherche(tab,elem):
    for e in tab:
        if e == elem:
            return True
    return False
recherche(tableau,2)
"""

#Taille des tableaux testés:
X = [50,100,200,400,800,1600]
Y = []

for t in X:
    ### [1] * t retourne un tableau de taille n ne contenant que des 1
    ### temps contient le temps d'exécution moyen em millisecondes du code
    temps =  timeit(stmt=code,setup="tableau = "+str([1]*t))
    Y.append(temps)
   
print(X,Y)

plt.plot(X,Y)
plt.ylabel('Temps en microsecondes')
plt.xlabel('Taille du tableau')
#Sauvegarde l'image dans le répertoire du programme
plt.savefig("tempsFonctionTaille.png")
#Affiche l'image
plt.show()

#################
### EXERCICE 5
#################

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 
            'g', 'h', 'i', 'j', 'k', 'l', 
            'm', 'n', 'o', 'p', 'q', 'r', 
            's', 't', 'u', 'v', 'w', 'x', 'y', 'z']