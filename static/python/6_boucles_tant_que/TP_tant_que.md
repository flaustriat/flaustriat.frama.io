# TP: boucles "Tant que"

### Exercice 1

Dans chacun des cas faire la _trace_ du programme, puis vérifier avec Spyder.

La trace devra contenir une colonne pour la variable  `i`, une autre pour la partie affichage et enfin une dernière colonne pour le test.

1.  
```python
j = 3
i = 0
while i <4:
	print(i+j)
	i = i + 1
```
2. 
```python
j = 3
i = 6
while i >4:
	print(i*j)
	i = i - 1
```
3. 
```python

i = 1
while i <7:
	print(i)
	i = i + 3
```

4.
 ```python


i = 8
while i % 5 != 0:  # Tant que i n'est pas un multiple de 5.
	print(i+j)
	i = i + 1
 ```

### Exercice 2

En informatique, __une procédure__ est une fonction qui ne renvoie pas de valeur. En Python une procédure est une fonction qui n'a pas d'instruction
 `return`, elle revoie alors `None`. Souvent les fonctions qui font uniquement de l'affichage sont des procédures.

_Un exemple_

```python	
def affiche_etoiles():
    """
    None -> None
    Affiche un triangle  de '*'sur quatre lignes
    """

	i = 0
	while i <4:
		i = i + 1
		print('*' * i) # affiche le caractère '*' i fois de suite
```
Est une procédure qui lorsque l'on l'appelle affiche:
~~~
*
**
***
****
~~~

1. Sur le même modèle écrire une procédure `affiche_etoiles(n)` qui affiche `n` lignes d'étoiles. C'est-à-dire que l'on ai:
```python
>>>affiche_etoiles(3)
*
**
***
```

2. Écrire une procédure `affiche_etoiles_renv(n)` telle que l'on obtienne:
```python
>>>affiche_etoiles_renv(4)
****
***
**
*
```

3. Écrire une procédure `affiche_diagonale(n)` telle que:

```python
>>>affiche_diagonale(4)
o
 o
  o
   o
```

### Exercice 3

Écrire une procédure `table_multiplication(n)` qui affiche la table de multiplication de `n`.
On doit avoir:

```python
>>>table_multiplication(3)
1 * 3 = 3
2 * 3 = 6
3 * 3 = 9
4 * 3 = 12
5 * 3 = 15
6 * 3 = 18
7 * 3 = 21
8 * 3 = 24
9 * 3 = 27
10 * 3 = 30
```


### Exercice 4 - multiplication russe

Le but de cette exercice est d'écrire une fonction `multiplication_russe(a,b)` qui calcule le produit des entiers `a` par `b` en utilisant la technique
dite de la multiplication russe.
Voir:
https://fr.wikipedia.org/wiki/Technique_de_multiplication_dite_russe

L'algorithme en Français:

> La technique de multiplication dite russe consiste à diviser 
> par 2 le multiplicateur (et ensuite les quotients obtenus), jusqu'à un quotient nul, 
> et à noter les restes ; et à multiplier parallèlement le multiplicande par 2.
>  On additionne alors les multiples obtenus du multiplicande correspondant aux restes non nuls. 


__Exemples__

Multiplions  32 par 5.

| 5 | 32 |
|---|---|
| $5 \div 2 = 2 \text{ reste } 1$ | $32$ |
| $2 \div 2 = 1 \text{ reste } 0$ | $32 \times 2 = 64$ |
| $1 \div 2 = 0 \text{ reste } 1$ | $64 \times 2 = 128$ |

On additionne les résultats de la deuxième colonne lorsque les restes sont non nuls. $32 + 128 = 160$.
On a donc bien $5 \times 32 = 160 $.

Multiplions 17 par 12

| 12 | 17 |
|---|---|
| $12 \div 2 = 6 \text{ reste } 0$ | $17$                |
| $6 \div 2 = 3 \text{ reste } 0$ | $17 \times 2 = 34$ |
| $3 \div 2 = 1 \text{ reste } 1$ | $34 \times 2 = 68$ |
| $1 \div 2 = 0 \text{ reste } 1$ | $68 \times 2 = 136$ |

On additionne les résultats de la deuxième colonne lorsque les restes sont non nuls. $68 + 136 = 204$.
On a donc bien $12 \times 17 = 204 $.


__L'algorithme__

```
prod = 0 // Contiendra le produit
Tant que b >0
	Si b % 2 est différent de 0
		prod = prod + a
	Fin Si
	a = a * 2
	b = b //2
Fin Tant que
Renvoyer prod
```


1. Écrire la fonction `multiplication_russe` et vérifier qu'elle fonctionne sur quelques exemples.
2. En quoi cette algorithme est-il intéressant à votre avis?

​		



 


