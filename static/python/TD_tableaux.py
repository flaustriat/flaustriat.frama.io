tab1 = ["Mesdames, messieurs","Je reste fondamentalement persuadé que", \
		"Dès lors, sachez que je me battrai pour faire admettre que", \
		"Par ailleurs, c'est en toute connaissance de cause que je peux affirmer aujourd'hui"]
tab2 = ["la conjoncture actuelle", "la situation d'exclusion connaissent", \
		"l'acuité des problèmes de la vie quotidienne",\
		   "la volonté farouche de sortir notre pays de la crise" ]
tab3 = ["doit s'intégrer à la finalisation globale", \
		"oblige à la prise en compte effective",\
		"interpelle le citoyen que je  et nous oblige tous à aller de l'avant dans la voie",\
		 "a pour conséquence obligatoire l'urgente nécessité" ]
tab4 = ["d'un processus allant vers plus d'égalité.",\
		"d'un avenir s'orientant vers plus de progrès et plus de justice.", \
		"d'une restructuration dans laquelle chacun pourra enfin retrouver sa dignité.", \
		"d'une valorisation sans concession de nos caractères spécifiques."]
        