#######
# Exemple 1 OpenFoodFacts
#######

import requests # Module permettant de faire des requêtes http

def produit(code_barre):
    """
    code_barre: int doit être un code barre valide
    Renvoie un dictionnaire contenant les données correspondant au code barre. 
    """
    url = "https://world.openfoodfacts.org/api/v0/product/"+str(code_barre)+".json"
    reponse = requests.get(url) # Requête GET de cette URL
    reponse_dict = reponse.json() # Converti la réponse en un dictionnaire Python
    return reponse_dict
donnees_produit = produit(3251320020637)
print("Code barre:",donnees_produit['code'])
print("Nom du produit:",donnees_produit['product']['generic_name'])
print("Nutriscore:",donnees_produit['product']['nutriscore_grade_producer'])
print("Sodium pour 100g:",donnees_produit['product']['nutriments']['sodium_100g'])


#####
# Exemple 2 : InfoClimat
#####

import requests
import matplotlib.pyplot as plt

paris = (48.85341,2.3488) # Coordonnées GPS de Paris
def meteo(coord):
    url = "http://www.infoclimat.fr/public-api/gfs/json?_ll="+ str(coord[0]) + "," + str(coord[1]) + "&_auth=BhwHEAZ4UHICL1ViDngLIgRsDjsLfQQjBnpSMVo%2FA35UP1U0B2dTNVA%2BB3oGKVZgBCkAY1phVGQKYVEpAXMFZAZsB2sGbVA3Am1VMA4hCyAEKg5vCysEIwZtUj1aKQNhVDFVMgd6UzNQPAdgBihWYAQzAH9aelRtCm9RNwFrBWAGZgdnBmdQNAJoVSgOIQs5BDcOaAtmBD4GZlJhWj4DMlQ3VTUHYVNgUDYHewYxVmQENwBnWmNUZAptUT8BcwV5BhwHEAZ4UHICL1ViDngLIgRiDjALYA%3D%3D&_c=e8a133f26c5f82ac2013ae79ce6ccc6e"
    reponse = requests.get(url) # Requête GET de cet URL
    # Convertie la réponse en une liste de dictionnaires Python
    reponse_dict = reponse.json()
    return reponse_dict
def kelvin2degre(temp):
    """temp: float: température en Kelvin
    Renvoie la température en Celsius arrondi au degré près
    """
    return round(temp - 273.15)
dico = meteo(paris)
x = []
y = []
x_ticks = [] # Affichage sur l'axe des abscisses
x_ticks_label = [] # Nom affiché sur l'axe des abscisses.
for cle in dico:
    if ':00' in cle: #sélectionne les clés qui contiennent ":00" a priori se sont des dates.
        donnees = dico[cle]
        temperatureK = donnees['temperature']['2m']
        temperature = kelvin2degre(temperatureK)
        x.append(cle)
        y.append(temperature)
        # Ajout uniquement lors des changements de jour
        if  len(x_ticks) == 0 or x_ticks_label[len(x_ticks)-1] != cle[:10]:
            x_ticks.append(cle)
            x_ticks_label.append(cle[:10])      
plt.plot(x,y)
plt.xlabel("Date")
plt.xticks(x_ticks,labels=x_ticks_label, rotation=20)
plt.ylabel("Température C°")
plt.title("Prévision méteo Paris")
plt.grid()
plt.show()


######
# Exemple 3 Covid Nombre de Morts en France
#####

import requests

url = "https://api.covid19api.com/dayone/country/france"
reponse = requests.get(url) # Requête GET de cet URL
# Convertie la réponse en une liste de dictionnaires Python
reponse_list = reponse.json() 
for donnees in reponse_list:
    # Selectionne les données de France Métropolitaine.
    if donnees['Province'] == "":
                print(donnees['Date'][:10]," nombre de morts",donnees['Deaths'] )
      